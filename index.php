<?php
   include('db.php');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Facebook login </title>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <link rel="shortcut icon" href="icon.png" />
  </head>
  <body>
    <?php
      if(!$user_SESSION['logged']){
        echo '<button type="button" name="button" class="fb_login">LOGIN</button>';
      }else{
        echo '<button type="button" name="button" class="fb_logout">LOGOUT</button>';
      }
    ?>
    <div class="account">

    </div>
    <script type="text/javascript">
    $.getJSON('log.php', function(data) {
                if(data.logged){
                  console.log(data);
                $('.account').html(  '<img  src="http://graph.facebook.com/'+data.user_data.id+'/picture" alt="photo">');
                }
              });
    $('.fb_login').click(function() {
      var new_window = window.open('<?php echo @$facebook_login_url; ?>', 'facebook-popup', 'height=350,width=600')
      var timer = setInterval(function() {
          if(new_window.closed) {
            location.reload()
              clearInterval(timer);
          }
      }, 1000);
    });
    $('.fb_logout').click(function() {
      var new_window = window.open('logout.php', 'facebook-popup', 'height=350,width=600')
      var timer = setInterval(function() {
          if(new_window.closed) {
            location.reload()
              clearInterval(timer);
          }
      }, 1000);
    });
    </script>
  </body>
</html>
